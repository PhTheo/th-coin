#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" Block class for the blockchain application """


from datetime import datetime
from collections import OrderedDict

from cryptography.hazmat.primitives import hashes

from transaction import Transaction

import logging

class Block:

    def __init__(self, index=0, transactions=[], prev_hash=0, difficulty=4):
        """
        generate a new block for the blockchain

        :param index:        number of the block in the chain
        :param transactions: list of transactions
        :param prev_hash:    hash value of previous block in the chain
        :param difficulty:   difficulty level for mining of this block
        """
        
        self.index = index
        self.timestamp = datetime.now()
        self.previous_block_hash = prev_hash
        self.verified_transactions = [t for t in transactions if self.is_valid_transaction(t) ]
        self.difficulty = difficulty
        self.nonce = None


    def to_dict(self):
        return OrderedDict({
            'index': self.index,
            'timestamp': str(self.timestamp),
            'transactions': [t.hash() for t in self.verified_transactions],
            'difficulty': self.difficulty,
            'nonce': self.nonce,
            'previous_hash': self.previous_block_hash
        })


    def hash(self):
        """
        compute SHA256 hash of block represented as dictionary,
        keys must be ordered lest the the hash becomes ambiguous
        """

        block_string = str(self.to_dict()).encode('utf8')
        return Transaction.hash_hexdigest(block_string)


    def is_valid_transaction(self, t):
        """
        a transaction is valid if
        1) sender's balance covers transaction
        2) sender's signature is valid

        :params t: transaction to be verified
        """

        # We do not enforce balanced accounts
        account_balanced = True

        # Check if signature is valid
        signature_valid = t.has_valid_signature()

        return account_balanced and signature_valid


    def is_valid(self):
        """
        true if hexadecimal representation of self.hash()
        starts with <leading_zeros> zeros
        """

        digest = self.hash()
        return digest.startswith("0"*self.difficulty)


    def mine(self):
        """
        Find a nonce such that `self.hash()` gets valid
        (i. e. starts with a certain number of leading zeros)
        """

        self.nonce = 0
        while not self.is_valid():
            self.nonce+= 1

        logging.debug(f'Nonce {self.nonce} found for block {self.index}')
        logging.debug(f'Hash: {self.hash()}')


    def dump(self):
        print('='*2, 'BLOCK', '='*67)
        print(f"index:      {self.index}")
        print(f"previous:   {self.previous_block_hash}")
        print(f"timestamp:  {self.timestamp}")
        print(f"nonce:      {self.nonce}")
        print(f"hash:       {self.hash()}")
        print(f"valid:      {self.is_valid()}")
        for transaction in self.verified_transactions:
            transaction.dump()


def test_block(difficulty=4):
    """ 
    test functionality of this module by creating a block 
    and mining it.
    """
    from client import Client
    from random import randrange
    
    richard = Client()
    dagmar = Client

    t = richard.create_signed_transaction(dagmar.identity, 10)
    b = Block(index=randrange(2**64), transactions=[t], 
              prev_hash=None, difficulty=difficulty)

    b.mine()
    b.dump()
    return b.is_valid()


if __name__ == '__main__':
    test_block()
