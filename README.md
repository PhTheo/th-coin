[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)  
[![made-with-python-doc](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://python.org/)  
[![Open Source? Yes!](https://badgen.net/badge/Open%20Source%20%3F/Yes%21/blue?icon=github)](https://github.com/Naereen/badges/)

# TH Coin

A simple application to demonstrate blockchain technology
