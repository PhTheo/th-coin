#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" measure mining times """

import csv

from block import Block
from datetime import datetime
from random import randrange


def source_file_hash(fpath = 'block.py'):
    """ 
    returns hash of file 'block.py' in hexadecimal form
    It is used for version control only, so it should be short 
    and does not need to be cryptographically secure
    """

    import hashlib, os

    if os.path.getsize(fpath) >= 2**64:
        logging.warning('File is bigger than 64kB, consider hashing in chunks')

    with open(fpath, 'rb') as f:
        md5 = hashlib.md5( f.read() )
        return md5.hexdigest()
    
def mining_time(difficulty, version=None):
    """
    generate a block with <transactions> and
    random block number and mine it with set difficulty

    :param difficulty:   difficulty level of the mining process
    :param transactions: list of transactions 
                         to be included into the test block
    :returns:            version sting, difficulty, timedelta object with execution time
    """

    if version is None:
        version = source_file_hash()
    rmax = 2**64
    b = Block(randrange(rmax), difficulty=difficulty)
    start = datetime.now()
    b.mine()
    stop = datetime.now()
    execution_time = stop - start

    return(version, difficulty, b.nonce, execution_time.total_seconds())


def write_csv(results):
    with open('mining_times.csv', 'a') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(results)


if __name__ == '__main__':
    from random import choices
    from tqdm import tqdm

    import argparse, signal

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--iterations", type=int,
                        default=100,
                        help="number of measurements")
    parser.add_argument("-d", "--max_difficulty", type=int,
                        default=5,
                        help="maximum difficulty level")
    args = parser.parse_args()

    print('Measuring mining times ...')

    block_version = source_file_hash('block.py')
    print('block.py hash:', block_version)

    results=[]
    difficulties = choices(range(args.max_difficulty+1), k=args.iterations)
    try:
        for d in tqdm(difficulties):
            results.append(mining_time(d, block_version))
    except KeyboardInterrupt:
        pass
    write_csv(results)
    


